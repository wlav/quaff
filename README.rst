.. -*- mode: rst -*-

Quaff: Python-C++ bindings generator for CFFI
=============================================

`CFFI <http://cffi.readthedocs.org>`_ provides a light-weight interface to
C code.
C++ can be used in Python through CFFI by providing wrappers with C-linkage.
However, that leads to extra maintenance and costs an additional indirection.
`Quaff <http://quaff.readthedocs.org>`_ automates the process and does not
require wrappers: starting from C++ header files, it generates the necessary
CFFI-based Python modules, calling C++ directly.

Full documentation: `quaff.readthedocs.org <http://quaff.readthedocs.org>`_.
