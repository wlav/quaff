.. quaff documentation master file, created by
   sphinx-quickstart on Wed Jul 12 14:35:45 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Quaff: Python-C++ bindings generator for CFFI
=============================================

`CFFI`_ provides a light-weight interface to C code.
C++ can be used in Python through CFFI by providing wrappers with C-linkage.
However, that leads to extra maintenance and costs an additional indirection.
Quaff automates the process and does not require wrappers: starting from C++
header files, it generates the necessary CFFI-based Python modules, calling
C++ directly.

.. _`CFFI`: http://cffi.readthedocs.org


Contents:

.. toctree::
   :maxdepth: 2

   installation


Comments and bugs
-----------------

Please report bugs or requests for improvement on the `issue tracker`_.

.. _`issue tracker`: https://bitbucket.org/wlav/quaff/issues
