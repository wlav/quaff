Installation
============

The ``quaff`` module, utilities, and dependencies, can be directly installed
from `PyPI`_, using pip::

 $ pip install quaff

Quaff supports PyPy, CPython2, and CPython3.

.. _`PyPI`: https://pypi.python.org/pypi/quaff/
