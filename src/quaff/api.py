from cffi import FFI
from cppyy_backend import loader


# load the backend library with all dependencies
_cppyy_backend = loader.load_cpp_backend()


# setup basic FFI interface to the API from a sub-set of cppyy/capi
_ffi = FFI()

_ffi.cdef("""typedef unsigned long cppyy_scope_t;
    typedef cppyy_scope_t cppyy_type_t;
    typedef unsigned long cppyy_object_t;
    typedef unsigned long cppyy_method_t;
    typedef long          cppyy_index_t;

    /* name to opaque C++ scope representation -------------------------------- */
    int cppyy_num_scopes(cppyy_scope_t parent);
    char* cppyy_scope_name(cppyy_scope_t parent, cppyy_index_t iscope);
    char* cppyy_resolve_name(const char* cppitem_name);
    cppyy_scope_t cppyy_get_scope(const char* scope_name);
    cppyy_type_t cppyy_actual_class(cppyy_type_t klass, cppyy_object_t obj);
    size_t cppyy_size_of(cppyy_type_t klass);

    /* scope reflection information ------------------------------------------- */
    int cppyy_is_namespace(cppyy_scope_t scope);
    int cppyy_is_template(const char* template_name);
    int cppyy_is_abstract(cppyy_type_t type);
    int cppyy_is_enum(const char* type_name);

    /* class reflection information ------------------------------------------- */
    char* cppyy_final_name(cppyy_type_t type);
    char* cppyy_scoped_final_name(cppyy_type_t type);
    int cppyy_has_complex_hierarchy(cppyy_type_t type);
    int cppyy_num_bases(cppyy_type_t type);
    char* cppyy_base_name(cppyy_type_t type, int base_index);
    int cppyy_is_subtype(cppyy_type_t derived, cppyy_type_t base);

    /* calculate offsets between declared and actual type, up-cast: direction > 0; down-cast: direction < 0 */
    ptrdiff_t cppyy_base_offset(cppyy_type_t derived, cppyy_type_t base, cppyy_object_t address, int direction);

    /* method/function reflection information --------------------------------- */
    int cppyy_num_methods(cppyy_scope_t scope);
    cppyy_index_t cppyy_method_index_at(cppyy_scope_t scope, int imeth);

    char* cppyy_method_name(cppyy_scope_t scope, cppyy_index_t idx);
    char* cppyy_method_mangled_name(cppyy_scope_t scope, cppyy_index_t idx);
    char* cppyy_method_result_type(cppyy_scope_t scope, cppyy_index_t idx);
    int cppyy_method_num_args(cppyy_scope_t scope, cppyy_index_t idx);
    int cppyy_method_req_args(cppyy_scope_t scope, cppyy_index_t idx);
    char* cppyy_method_arg_type(cppyy_scope_t scope, cppyy_index_t idx, int arg_index);
    char* cppyy_method_arg_default(cppyy_scope_t scope, cppyy_index_t idx, int arg_index);

    int cppyy_method_is_template(cppyy_scope_t scope, cppyy_index_t idx);
    int cppyy_method_num_template_args(cppyy_scope_t scope, cppyy_index_t idx);
    char* cppyy_method_template_arg_name(cppyy_scope_t scope, cppyy_index_t idx, cppyy_index_t iarg);
 
    /* method properties ------------------------------------------------------ */
    int cppyy_is_publicmethod(cppyy_type_t type, cppyy_index_t idx);
    int cppyy_is_constructor(cppyy_type_t type, cppyy_index_t idx);
    int cppyy_is_destructor(cppyy_type_t type, cppyy_index_t idx);
    int cppyy_is_staticmethod(cppyy_type_t type, cppyy_index_t idx);

    /* data member reflection information ------------------------------------- */
    int cppyy_num_datamembers(cppyy_scope_t scope);
    char* cppyy_datamember_name(cppyy_scope_t scope, int datamember_index);
    char* cppyy_datamember_type(cppyy_scope_t scope, int datamember_index);
    ptrdiff_t cppyy_datamember_offset(cppyy_scope_t scope, int datamember_index);

    int cppyy_datamember_index(cppyy_scope_t scope, const char* name);

    /* data member properties ------------------------------------------------- */
    int cppyy_is_publicdata(cppyy_type_t type, int datamember_index);
    int cppyy_is_staticdata(cppyy_type_t type, int datamember_index);

    /* misc helpers ----------------------------------------------------------- */
    void cppyy_free(void* ptr);
""")


# import the functions
_ffi_backend = _ffi.dlopen('libcppyy_backend.so')


#
### decorate and publish the API
#

# global namespace scope
GLOBAL = 1


# typedefs
cppyy_scope_t  = long
cppyy_type_t   = cppyy_scope_t
cppyy_object_t = long
cppyy_method_t = long
cppyy_index_t  = long


# helpers
def _cstr2pstr_free(cstr):
    if not cstr:
        return ''
    pystr = _ffi.string(cstr)
    free(cstr)
    return pystr


# name to opaque C++ scope representation
num_scopes = _ffi_backend.cppyy_num_scopes
#def scope_name(parent, iscope):
#    return ffi.string(_ffi_backend.cppyy_scope_name(parent, iscope))

def resolve_name(name):
    return _cstr2pstr_free(_ffi_backend.cppyy_resolve_name(name))

get_scope    = _ffi_backend.cppyy_get_scope
#actual_class = _ffi_backend.cppyy_actual_class
size_of      = _ffi_backend.cppyy_size_of


# method/function reflection information
num_methods     = _ffi_backend.cppyy_num_methods
method_index_at = _ffi_backend.cppyy_method_index_at

def method_name(scope, index):
    return _cstr2pstr_free(_ffi_backend.cppyy_method_name(scope, index))
def method_mangled_name(scope, index):
    return _cstr2pstr_free(_ffi_backend.cppyy_method_mangled_name(scope, index))
def method_result_type(scope, index):
    return _cstr2pstr_free(_ffi_backend.cppyy_method_result_type(scope, index))
method_num_args = _ffi_backend.cppyy_method_num_args
#method_req_args = _ffi_backend.cppyy_method_req_args
def method_arg_type(scope, index, arg_index):
    return _cstr2pstr_free(
       _ffi_backend.cppyy_method_arg_type(scope, index, arg_index))


# method properties
is_publicmethod = _ffi_backend.cppyy_is_publicmethod
is_constructor  = _ffi_backend.cppyy_is_constructor
is_destructor   = _ffi_backend.cppyy_is_destructor
is_staticmethod = _ffi_backend.cppyy_is_staticmethod


# misc helpers
free = _ffi_backend.cppyy_free
