from . import api

def generate_class(module, klass, library):
    if 3 < len(module) and module[-3:] != '.py':
        module = module + '.py'

  # extract class
    scope = api.get_scope(klass)
    if not scope:
        raise TypeError("%s does not exist/is not loaded" % klass)

  # collect methods
    stats = []
    ctors = []
    dtors = []
    meths = []

    for i in xrange(api.num_methods(scope)):
        j = api.method_index_at(scope, i)

        if not api.is_publicmethod(scope, j):
            continue

        if api.is_staticmethod(scope, j):
            stats.append(j)
        elif api.is_constructor(scope, j):
            ctors.append(j)
        elif api.is_destructor(scope, j):
            dtors.append(j)
        else:
            meths.append(j)

  # write
    f = open(module, 'w')

  # ffi preamble
    f.write("""from cffi import FFI

_ffi = FFI()

_ffi.cdef(\"\"\"\n""")

  # constructor definitions
    for i in ctors:
        acceptable = True
        for j in xrange(api.method_num_args(scope, i)):
            if '&' in api.method_arg_type(scope, i, j):
                acceptable = False
        if not acceptable:
            continue

        f.write("void %s(void*" % api.method_mangled_name(scope, i))
        for j in xrange(api.method_num_args(scope, i)):
            f.write(", %s" % api.method_arg_type(scope, i, j))
        f.write(");\n")

  # method definitions
    for i in meths:
        if api.method_name(scope, i) == 'operator=':
            continue
        f.write("%s %s(void*" % (api.method_result_type(scope, i),
                                 api.method_mangled_name(scope, i)))
        for j in xrange(api.method_num_args(scope, i)):
            f.write(", %s" % api.method_arg_type(scope, i, j))
        f.write(");\n")

  # close cdef
    f.write("\"\"\")\n\n")

  # load library
    f.write("_dll = _ffi.dlopen('./%s')\n\n" % library)

    f.write("class %s(object):\n" % klass);

  # python-side constructor wrappers
    f.write("  def __init__(self, *args):\n")
    f.write("    self._cpp_this = _ffi.new('char[]', %d)\n" % api.size_of(scope))
    for i in ctors:
        acceptable = True
        for j in xrange(api.method_num_args(scope, i)):
            if '&' in api.method_arg_type(scope, i, j):
                acceptable = False
        if not acceptable:
            continue

        f.write("    if len(args) == %d:\n" % api.method_num_args(scope, i))
        f.write("      _dll.%s(self._cpp_this, *args)\n" % \
                                          api.method_mangled_name(scope, i))
       

  # python-side method wrappers
    for i in meths:
        name = api.method_name(scope, i)
        if name == 'operator=':
            continue
        f.write("  def %s(self, *args):\n" % name)
        f.write("    return _dll.%s(self._cpp_this, *args)\n" % \
                         api.method_mangled_name(scope, i))

  # done
    f.close()
