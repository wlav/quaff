#include "Simple.h"

//===========================================================================
Simple::Simple() : m_data(42) {

}

Simple::Simple(int i) : m_data(i) {

}

Simple::~Simple() {

}

int Simple::get_data() {
    return m_data;
}

void Simple::set_data(int i) {
    m_data = i;
}
