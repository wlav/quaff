class Simple {
public:
    Simple();
    Simple(int);
    virtual ~Simple();

public:
    int get_data();
    void set_data(int);

private:
    int m_data;
};

typedef Simple Simple_t;
