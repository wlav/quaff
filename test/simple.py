from cffi import FFI

_ffi = FFI()

_ffi.cdef("""
void _ZN6SimpleC1Ev(void*);
void _ZN6SimpleC1Ei(void*, int);
int _ZN6Simple8get_dataEv(void*);
void _ZN6Simple8set_dataEi(void*, int);
""")

_dll = _ffi.dlopen('./libsimple.so')

class Simple(object):
  def __init__(self, *args):
    self._cpp_this = _ffi.new('char[]', 16)
    if len(args) == 0:
      _dll._ZN6SimpleC1Ev(self._cpp_this, *args)
    if len(args) == 1:
      _dll._ZN6SimpleC1Ei(self._cpp_this, *args)
  def get_data(self, *args):
    return _dll._ZN6Simple8get_dataEv(self._cpp_this, *args)
  def set_data(self, *args):
    return _dll._ZN6Simple8set_dataEi(self._cpp_this, *args)
