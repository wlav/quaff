import py, os, sys
from pytest import raises

class TestAPI:
    def setup_class(cls):
      # TODO make this a backend API
        import cppyy
        cppyy.gbl.gInterpreter.LoadMacro('Simple.h')

    def test01_basic_api_calls(self):
        """Availability and functioning of the cppyy_backend API"""

        from quaff import api

        assert api.num_scopes(api.GLOBAL) != 0

        simple_scope = api.get_scope('Simple')
        assert simple_scope != 0
        assert api.num_scopes(simple_scope) == 0
        assert api.get_scope('Simple_t') == simple_scope

        assert api.resolve_name('Simple')   == 'Simple'
        assert api.resolve_name('Simple_t') == 'Simple'

        assert api.num_methods(simple_scope) == 7
        stats = []
        ctors = []
        dtors = []
        meths = []

        for i in xrange(api.num_methods(simple_scope)):
            j = api.method_index_at(simple_scope, i)
            assert i == j     # not required, just happens to be the case

            if not api.is_publicmethod(simple_scope, j):
                continue

            name = api.method_name(simple_scope, j)
            if api.is_staticmethod(simple_scope, j):
                stats.append(name)
            elif api.is_constructor(simple_scope, j):
                ctors.append(name)
            elif api.is_destructor(simple_scope, j):
                dtors.append(name)
            else:
                meths.append(name)

        assert len(stats) == 0
        assert len(ctors) == 3 # includes cctor
        assert len(dtors) == 1
        assert len(meths) == 3 # includes operator=
