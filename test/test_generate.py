import py, os, sys
from pytest import raises

class TestGENERATION:
    def setup_class(cls):
      # TODO make this a backend API
        import cppyy
        cppyy.gbl.gInterpreter.LoadMacro('Simple.h')

    def test01_module_generation(self):
        """Generate a module and verify the class inside"""

        from quaff import generate

        raises(TypeError, generate.generate_class, 'simple', 'does_not_exist')

        generate.generate_class('simple', 'Simple', 'libsimple.so')

        if not os.path.exists('libsimple.so'):
            os.system("g++ -shared -o libsimple.so -fPIC Simple.cxx")

        import simple
        assert simple.Simple

        s1 = simple.Simple()
        assert s1.get_data() == 42
        s1.set_data(13)
        assert s1.get_data() == 13

        s2 = simple.Simple(99)
        assert s2.get_data() == 99
